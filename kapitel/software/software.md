# Online-Formate:  Software & Services

Online-Veranstaltungen benötigen entweder eine Software wie Zoom, MS Teams, Jitsi, BigBlueButton oder eine Lernplattform wie moodle um stattfinden zu können. 
Während im vorangegangen Kapitel Techniken und Methoden für diese Plattformen Thema waren, sollen nun Programme und Services vorgestellt werden, die zusätzlich gute Dienste leisten können, die dabei aber für die Teilnehmer:innen in der Regel nicht sichtbar sind. 

## Handbrake
Handbrake (www.handbrake.fr) ist ein offener, freier und kostenloser Video-Konverter, der auch noch leicht zu bedienen ist. Verfügbar ist handbrake für alle gängigen Betriebssysteme: Linux, Mac, Windows. Die Stärke von handbrake ist, dass damit eine Konvertierung fast jedes Videoformats möglich ist. Zudem kann die Auflösung eures Videos angepasst werden sowie eine Web-Optimierung vorgenommen werden: 

![Handbrake](/images/handbrake.png)
*Screenshot: handbrake. Alle Rechte bei handbrake.fr. Bearbeitung: CC BY-SA Christian Pfliegel*

Was hat ein Videokonverter wie Handbrake mit Online-Veranstaltungen zu tun? Es gibt gute Gründe, Vorträge von Referent:innen im Vorfeld aufzuzeichnen und diese dann bei der Veranstaltung per Bildschirmfreigabe zu zeigen: Zum einen ist es sicherer, da der Vortrag nicht ausfallen muss, sollte die Verbindung zum Vortragenden abreißen. Zum anderen ist genau planbar, wie lange der Vortrag dauert und gleichzeitig habt ihr als Veranstalter:in eine kurze Pause, die ggfs. für Absprachen genutzt werden kann. 
Häufig sind Aufzeichnungen, die ihr zugeschickt bekommt, entweder in einem Format, das nicht optimal auf eurem Rechner läuft oder die Auflösung und die Größe sind zu groß für eine Einspielung per Bildschirmfreigabe. In diesem Fall bietet es sich an, die Aufzeichnung vorher mit Hilfe von Handbrake zu optimieren. 
In der Praxis haben sich (bei einer durchschnittlichen Bandbreite von 20 bis 100) folgende Einstellungen für eine Bildschirmfreigabe bewährt: 

* Format: mp4
* Web-Optimiert: ja
* Voreinstellung: General => Fast 720p30

Mit dieser Voreinstellung erhaltet ihr ein Video, das nicht zu groß für eine Übertragung ist, das aber dennoch ausreichende Bildqualität liefert, auch wenn  Folien oder Texte gezeigt werden. Solltet ihr eine eher langsame Internetverbindung haben oder enthält euer Video keinen Text, kann auch mit einer 480p30-Einstellung gearbeitet werden. Enthält euer Video Ton, bei der Freigabe die beiden Haken für „Video-Optimierung“ und „Ton-Freigabe“ nicht vergessen (bei Zoom)!

**Praxistipp:**
Solltet ihr unsicher sein, wie gut eure Internetverbindung ist, empfiehlt sich vorher dringend ein Test, bei dem ihr das Video mal freigebt. Häufig ist die Geschwindigkeit von Internetverbindungen asynchron, d. h. die Downloadgeschwindigkeit ist deutlich höher als die Uploadgeschwindigkeit (teils um den Faktor 3 oder mehr). Für die Freigabe per Screensharing ist die Uploadgeschwindigkeit entscheidend, weshalb es hier schnell zu einer Überlastung der Leitung kommen kann. Um Bandbreite zu sparen kann es helfen, während der Videofreigabe euer Video auszuschalten!


## Telegraph
Für die Teilnehmer:innen einer Online-Veranstaltung ist es, wie weiter vorne bereits geschrieben, eine große Hilfe, wenn alle relevanten Informationen auf einer Webseite stehen, die bereits im Vorfeld verschickt wird. Auf eine solche Webseite gehören z. B. der Link zum Online-Raum, das Programm, Links zu Whiteboards, eine Beschreibung von Gruppenaktivitäten, eventuell der Link zur Evaluation usw.
Das Problem hierbei ist, dass nicht jede:r einfach für eine Veranstaltung eine Webseite erstellen kann, Pads wie etherpad und Co. bieten häufig zu wenige Einstellungs- und Layoutmöglichkeiten. Gute Dienste kann hier der Webservice telegra.ph bieten (die Adresse bitte genau so in euren Browser eingeben). Telegraph ist ein freies Webangebot zur einfachen Erstellung von Webseiten: 

![Telegraph](/images/telegraph.png)
*Screenshot telegra.ph. Alle Rechte bei Telegraph*

Die Seite ist sehr minimalistisch gehalten, die Bedienung ist schnell zu erlernen und selbsterklärend. Es können Links, Texte und Bilder ganz einfach eingebunden werden, so dass eine Seite gestaltet werden kann, die optisch sehr ansprechend ist, inklusive Links, Logos und Formatierungen. Im Anschluss einfach auf „Publish“ klicken und den Link mit den Teilnehmer:innen teilen. 

**Praxistipp:**
Telegraph kann auch sehr gut in Online-Veranstaltungen eingebaut werden: Teilnehmer:innen können mit diesem Tool Arbeitsergebnisse aus der Kleingruppenarbeit vorstellen. Ich handhabe es in der Praxis so, dass ich den Teilnehmer:innen miro.com/lite/, flinga.fi und telegra.ph zu Beginn vorstelle. Sie können dann selbst entscheiden, mit welchem Angebot sie arbeiten wollen (oder auch mit einem eigenen Tool).


## OBS = Open Broadcast Studio
Open Broadcast Studio, oder kurz OBS, ist ein Programm, aus dem zum einen direkt gestreamt werden kann (YouTube, Vimeo, Twitch und viele weitere), zum anderen kann damit auch lokal aufgezeichnet werden. OBS ist Open-Source und auf allen gängigen Desktop-Betriebssystemen verfügbar. 
OBS hat einen großen Funktionsumfang, der durch Plugins noch erweitert werden kann. Im Kontext von Online-Veranstaltungen ist aber vor allem die Screencast-Funktion (= parallele Aufzeichnung des Bildschirms + Audiospur) interessant: Mit OBS ist es sehr einfach, alleine einen Vortrag inklusive Präsentation, ein Kamerabild des Vortragenden und eine zugehörige Audiospur in einem Schritt aufzunehmen, ohne dass eine komplexe und langwierige Nachbearbeitung notwendig wird. 
Die Bedienung ist dabei recht schnell zu erlernen: 
Die Zusammenstellung von Videoquelle, Audioquelle, Präsentation usw. wird über sog. Szenen vorgenommen, wobei jede Szene eine Zusammenstellung verschiedener Quellen ist: 

![OBS](/images/obs.png)
*Die Oberfläche von OBS. Eigener Screenshot. Alle Rechte bei OBS*

Auf der linken Seite kannst du dir deine Zusammenstellung der Quellen (Bildschirmansicht, Präsentation, Videoquellen, Audio, Text usw.) anschauen, auf der rechten Seite siehst du die eigentliche Aufnahme. 

Der Wechsel von Vorschau auf Aufnahme (bzw. Programm) erfolgt mittels des Schiebereglers in der Mitte. Rechts unten wird der Stream bzw. die Aufnahme gestartet, zudem können hier die Einstellungen vorgenommen werden. 
Folgende Einstellungen haben sich für Aufzeichnungen, die für Online-Veranstaltungen erstellt werden, bewährt: 

Videobitrate: zwischen 1000 und 1200
Videoformat: mp4
Videoqualität: gleich wie Stream
Basis-Video-Auflösung (unter dem Menüpunkt Video): 1280 x 720
Frames-per-second (FPS): 30

Da OBS ein sehr verbreitetes Programm ist finden sich beispielsweise bei YouTube zahlreiche empfehlenswerte Tutorials in unterschiedlicher Ausführlichkeit. 

## Freie Software Onlinedienste: Chatons
Manchmal kann es im Rahmen von Online-Veranstaltungen sehr praktisch sein, auf die Schnelle ein Etherpad anzulegen, einen Termin zu finden oder datensicher Fotos oder Dateien zu sammeln. Sehr hilfreich bei all diesen Aufgaben kann die Seite Chatons (https://entraide.chatons.org/de/) sein. 

Von dieser Seite aus können zahlreiche freie Angebote direkt und ohne Vorkenntnisse gestartet werden: 

![Chatons](/images/chatons.png)
*Chatons. Eigener Screenshot. Alle Rechte bei Chatons*


Die Möglichkeiten der Seite sind im Grund selbsterklärend, ein Blick lohnt sich vor allem in der Vorbereitungsphase einer Online-Veranstaltung, da die Angebote eventuell gut in das Programm eingebunden werden können. 

## Wonder - Interaktive Konferenzen
Zum Abschluss dieses Kapitels möchte ich noch kurz Wonder (www.wonder.me) vorgestellen, ein „interaktives“ Videosystem. Der Funktionsumfang von Wonder ist mit Zoom, Jitsi oder BigBlueButton vergleichbar, durch den Aufbau sind jedoch spannende Methoden möglich. 

### Was ist Wonder?
Wonder ist ein (derzeit noch?) kostenloses Videokonferenztool. Die Besonderheit ist, dass sich die Nutzer:innen frei auf dem Bildschirm bewegen können, um sich ihre Gesprächspartner:innen auszusuchen. Der Vorteil liegt darin, dass so Gruppenbildungen im Vergleich zu den anderen Plattformen leichter möglich sind. Dadurch eignet sich die Seite sehr gut für informelle Treffen, Partys und Flurgespräche (die Bedeutung von informellen Gespräche und sozialem Austausch für das Gelingen von Online-Veranstaltungen darf nicht unterschätzt werden!). 

### Wonder: Technische Voraussetzungen
Die technischen Mindestvoraussetzungen für die Nutzung von Wonder sind Folgende: 

Mindestvoraussetzung: PC/Notebook
nach Möglichkeit: Chrome/Chromium/MS Edge. Auch firefox funktioniert,  eventuell aber mit Fehlern, da die Plattform nicht für diesen optimiert ist. 
Webcam (nach Möglichkeit) + Mikrofon
auf Handys/Tablets funktioniert Wonder derzeit gar nicht oder nur eingeschränkt!
Wonder muss nicht installiert werden, da es direkt im Browser läuft. Der Link muss einfach nur mit den Teilnehmer:innen geteilt werden (analog zu Jitsi). 

### Was spricht für die Benutzung von Wonder?
Wonder ist derzeit (März 2021) kostenlos und jede:r kann sich einen Account holen und mit diesem Veranstaltungen durchführen. Für einen Account einfach auf der Seite mit einer Mail-Adresse registrieren („Get wonder now“). Wonder ist ein deutsches Unternehmen mit Sitz in Berlin und somit an die deutschen Datenschutzbestimmungen gebunden. 

Der größte Vorteil ist jedoch, dass die Plattform mehr Interaktivität bietet als die Mitbewerber:innen, was eine gewisse Flexibilität für die Nutzer:innen mit sich bringt: Diese können sehr einfach selbst entscheiden, in welchen Raum sie wollen, bzw. mit wem sie sprechen möchten. Außerdem hat Wonder ein paar spannende Features eingebaut, die nur diese Plattform hat: eine Anpassbarkeit der Hintergründe und Räume, eine Einstiegsfrage („icebreaker“), die beantwortet werden kann vor dem Eintritt in den Raum sowie die Gesprächskreise („circle“), die auch geschlossen werden können, sodass anderen Nutzer:innen temporär der Zutritt verwehrt wird. 

### Was sind die Nachteile von Wonder?
Wonder ist derzeit (März 2021) noch in einer Testphase. Dadurch kann es immer wieder zu Fehlern kommen, auch die Unterstützung von mobilen Geräten fehlt (noch?). Zudem ist die Seite nur auf Englisch verfügbar. Der größte Vorteil von Wonder ist gleichzeitig der größte Nachteil: die Teilnehmer:innen - Host - Hierarchie ist weniger streng. Dies führt zu mehr Flexibilität, gleichzeitig hat der Host aber auch weniger Möglichkeiten, die Teilnehmer:innen zu steuern. Stumm schalten geht, aus dem Raum entfernen geht auch, aber z. B. die Einteilung in Breakouträume geht nicht. Diese müssen/können von den Teilnehmer:innen selbst aufgesucht werden. 

### Die Oberfläche von Wonder
Die Oberfläche von Wonder ist relativ simpel und selbsterklärend gehalten, die Anzahl der Einstellungsmöglichkeiten ist überschaubar. Klickt ein Teilnehmer den Einladungslink an, wird ein Foto mit der Webcam von ihm gemacht. Dieses Foto ist dann sein Avatar (das Foto kann später noch unter Einstellungen geändert werden). 

![Wonder](/images/wonder_1.png)
*Die Oberfläche von wonder.me. Anmerkungen und Screenshot von Christian Pfliegel. Alle Rechte bei Wonder.*

![Wonder](/images/wonder_2.png)
*Die Videoansicht von wonder.me. Anmerkungen und Screenshot von Christian Pfliegel. Alle Rechte bei Wonder.*


### Ideen & Methoden für den Praxiseinsatz von Wonder
Folgende Ideen und Methoden wurden in der Praxis getestet und von den Teilnehmer:innen gut bewertet: 

* **World-Café:** Die Referent:innen sind an verschiedenen Stellen auf dem Bildschirm positioniert, die Teilnehmer:innen gehen je nach Interesse in einem definierten Zeitintervall von Vortrag zu Vortrag. Zum Abschluss gibt es eine große, gemeinsame Runde, in der die Inhalte diskutiert werden. 
Kennenlern-Bingo: Die Teilnehmer:innen bekommen eine virtuelle Bingo-Karte, auf der 9 verschiedene Eigenschaften in einem Quadrat stehen („Vegetarier:in“, „Student:in“, „Spielt ein Musikinstrument“ oder ähnliches). Nun müssen sie ins Gespräch kommen und eine Personen finden, die die genannten Eigenschaften haben. Wer zuerst eine Reihe voll hat, ist Sieger:in!

* Kreise mit Expert:innen, die gezielt von den Teilnehmer:innen aufgesucht werden können, um einen Rat zu bekommen. 

* Pausen, oder Flurgespräche, virtuelle Kaffeetische.

Durch die Flexibilität der Plattform gibt es bestimmt noch zahlreiche weitere Methoden. Sei einfach kreativ!

## Canva und Cocomaterial
Bei der Durchführung von Online-Veranstaltungen gibt es immer wieder Bedarf an Dokumenten wie Arbeitsblättern, Vorlagen für Arbeitsaufträge, Einladungen  usw. Leider ist nicht jede:r ein:e geboren:e Designer:in oder hat die entsprechende Ausstattung mit Software. Zudem darf nicht jedes Bild, das im Internet gefunden werden kann, rechtlich sauber verwendet werden. 

Sehr gute Dienste leisten in einem solchen Fall die beiden Webseiten von www.cocomaterial.com und www.canva.com: Die Seiten sind eine gute und kostenlose Quellen für Bilder (Cocomaterial), bzw. Vorlagen (Canva) für alle möglichen Dokumente. Damit gelingt es auch Menschen mit überschaubarem künstlerischem Talent, ansehnliche Dokumente zu erstellen, die Spaß machen.
