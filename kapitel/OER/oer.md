# Open Educational Rescources (OER)
Vorbemerkung: Diese Kapitel ist ein Remix aus der Präsentation „OER in der Hochschule“, die von Julia Joachim erstellt wurde und unter einer CC BY-SA 4.0 - Lizenz steht. Der Link zur Originalquelle findet sich im Anhang! Texte und Bilder aus anderen Quellen sind direkt angegeben und finden sich ebenfalls im Anhang als Übersicht.

![OER](/images/oer.jpg)
*Abbildung: „OER is sharing“ von Giulia Forsythe unter CC 0 auf Flickr unter https://www.flickr.com/photos/gforsythe/38088290601/*

## Was sind OER?
Open Educational Resources (OER) sind Bildungsmaterialien jeglicher Art und in jedem Medium, die unter einer offenen Lizenz veröffentlicht werden. Eine solche offene Lizenz ermöglicht den kostenlosen Zugang sowie die kostenlose Nutzung, Bearbeitung und Weiterverbreitung durch andere ohne oder mit geringfügigen Einschränkungen.
Open Educational Resources können einzelne Materialien, aber auch komplette Kurse oder Bücher umfassen. Jedes Medium kann verwendet werden. Lehrpläne, Kursmaterialien, Lehrbücher, Streaming-Videos, Multimediaanwendungen, Podcasts – all diese Ressourcen sind OER, wenn sie unter einer offenen Lizenz veröffentlicht werden. (Quelle: UNESCO Definition)

![Logo-OER](/images/logo_oer.jpg)
<br>
*Abbildung: „Global OER Logo“ von Jonathas Mello unter CC BY 3.0 (via UNESCO)*

[Quelle dieses Abschnitts: „Was sind OER“ von Team OERinfo für OERinfo – Informationsstelle OER unter CC BY 4.0. Der Text wurde von Julia Joachim gekürzt. Link: https://open-educational-resources.de/was-ist-oer-3-2/.]

## Warum OER
Open Educational Resources (OER) weisen für die (Hochschul-)Lehre und andere Bildungsbereiche große Potenziale auf. Die Vorteile der Digitalisierung, die durch die gängige Handhabung des Urheberrechts häufig nicht zur Geltung kommen, können durch die offene Lizenzierung von Materialien voll ausgeschöpft werden. Lehrende und Studierende können stärker kollaborativ an und mit Materialien arbeiten. Lehrende können OER frei abrufen, für die eigene Lehre anpassen und dadurch einen Qualitätszuwachs erzielen. Hochschulen können ihren gesellschaftlichen Beitrag besser öffentlich sichtbar machen und breitere Zielgruppen ansprechen. Kurzum: Zahlreiche Verbesserungsprozesse können durch OER angestoßen werden.

Die Vision hinter OER liegt darin, eine stärker vernetzte Kultur des Lehrens und Lernens zu schaffen, Lernende stärker in die Produktion von Material einzubinden und Anstöße für innovative Lehr- und Lernszenarien zu schaffen. Auf diese Weise können OER dazu beitragen, dass Bildungsangebote entstehen, die individuellen Lernvoraussetzungen innerhalb von heterogenen Gruppen gerecht werden.

Die Kennzeichnung von Materialien mit einer offenen Lizenz und das Hochladen ebendieser auf offenen Plattformen im Netz stellt allerdings nur eine Rahmenbedingung für eine Veränderung der Lehre dar. Die weitaus größere Herausforderung ist es, die offenen Materialien zur Verbesserung der Lehre effektiv zu nutzen. Denkanstöße gibt es im Netz viele und die offenen Lizenzen ermöglichen, dass aus Materialien immer wieder neues Wissen entsteht. Dabei kann auch für Lehrende, die OER veröffentlichen, durchaus ein Anreiz darin bestehen, die Folgeprodukte der eigenen Arbeit zu betrachten. Dies ist einerseits als Feedback wie auch als Inspiration zu verstehen.

[Quelle dieses Abschnitts: „OER in der Hochschulbildung“ von David Eckhoff, Universität Duisburg Essen, für OERinfo – Informationsstelle OER unter CC BY 4.0 auf https://open-educational-resources.de/dossierseite/?praxis=allgemein&bereich=hochschule.  Der Text wurde von Julia Joachim gekürzt.]


## Die 5R/5V und die soziale Komponente
Die Veröffentlichung von Bildungsmaterialien unter einer offenen Lizenz ermöglicht einen Austausch sowie eine Bündelung von Ressourcen. Diese Möglichkeiten werden häufig als die 5R, bzw. die 5V im Deutschen genannt:

- **Retain (verwahren, vervielfältigen):** das Recht, Kopien des Inhalts anzufertigen, zu besitzen und zu kontrollieren (z. B. Download, Speicherung, Vervielfältigung). Im Gegensatz zu Materialien mit einer restriktiveren Lizenz können OER somit auch gut archiviert und gespeichert werden.

- **Reuse (verwenden):** das Recht, den Inhalt in unterschiedlichen Zusammenhängen einzusetzen (z. B. im Klassenraum, in einer Lerngruppe, auf einer Webseite, in einem Video … oder in einer Online-Veranstaltung).

- **Revise (verarbeiten):** das Recht, den Inhalt zu bearbeiten, zu verändern oder umzugestalten (z. B. übersetzen). Vor allem auch im internationalen Kontext kann dies ein großer Vorteil sein.  

- **Remix (vermischen):** das Recht, einen Inhalt im Original oder in einer Bearbeitung mit anderen offenen Inhalten zu verbinden und aus ihnen etwas Neues zu schaffen (z. B. beim Einbauen von Bildern und Musik in ein Video).

- **Redistribute (verbreiten):** das Recht, Kopien eines Inhalts mit anderen zu teilen, im Original oder in eigenen Überarbeitungen (z. B. eine:r Freund:in eine Kopie zu geben oder online zu veröffentlichen).

In der Praxis bieten OER damit den Vorteil, dass durch diese die Anpassbarkeit an die jeweiligen Bedürfnisse gegeben ist.

[Quelle 5R: CC BY Jöran Muus-Merholz, Jöran: Zur Definition von „Open“ in „Open Educational Resources“ – die 5 R-Freiheiten nach David Wiley auf Deutsch als die 5 V-Freiheiten.
Online verfügbar unter http://open-educational-resources.de/tag/david-wiley/]

Untrennbar verbunden mit dem offenen Gedanken des Teilens ist die soziale Komponente von OER. Die Beschäftigung von Lehr- und Lernmaterialien und deren Bearbeitung ermöglicht den Nutzer:innen ganz nach den persönlichen Bedürfnissen und ihrem Lernniveau angepasst zu lernen. Vor allem durch die Modifizierung der Quellen können ganz neue Formen der Zusammenarbeit entstehen.

Hinzu kommt, dass OER durch ihre digitale Bereitstellung den Zugang zu Wissen auf einem wesentlich niederschwelligeren Niveau ermöglichen. Handyneze und somit der Zugang zu Wissen kommen heute bereits bis in weit entlegene Gebiete, in denen sonst nur schwer oder kein Zugang zu Wissen und Bildung bestehen würde. Dies führt zu einer Bildungsgerechtigkeit auch für Personengruppen, denen der Zugang zu Bildung sonst auf Grund ihrer finanziellen Möglichkeiten oder politischer Verhältnisse verwehrt wäre. Die UNESCO führt dazu aus:

„UNESCO believes that universal access to high quality education is key to the building of peace, sustainable social and economic development, and intercultural dialogue. Open Educational Resources (OER) provide a strategic opportunity to improve the quality of education as well as facilitate policy dialogue, knowledge sharing and capacity building…” CC BY UNESCO, (http://www.unesco.org/new/en/communication-and-information/access-to-knowledge/open-educational-resources)

[Quelle dieses Abschnitts: CC BY Leibniz-Forschungsverband, handbuch.io. https://handbuch.tib.eu/w/Leitfaden_zu_Open_Educational_Resources_für_Bibliotheken_und_Informationseinrichtungen/Einleitung#Die_soziale_Komponente_von_OER]


## Qualität von OER
Viele Lehrende sorgen sich um die Qualitätsstandards von OER, die anders als z. B. gedruckte Bücher oder Journalartikel keine professionelle Qualitätsprüfung durchlaufen. Es gibt keine zentralen Prüfinstanzen oder Qualitätssiegel. Tatsache ist jedoch auch, dass veröffentlichte Materialien ebenso wenig eine hundertprozentige Qualitätsgarantie versprechen können. Zudem haben Community-Projekte wie Wikipedia bewiesen, dass Qualität auch außerhalb der gewohnten Bahnen möglich ist. Gerade die allgemeine Verfügbarkeit von OER (zumeist online) führt zu einer fortlaufenden „Schwarm-Qualitätskontrolle“ und stetiger Weiterentwicklung.

Hochschullehrende sind bei der Verwendung von OER letztlich ebenso wie bei der Verwendung „klassisch“ veröffentlichter Materialien gefordert, diese auf Glaubwürdigkeit, Qualität und Nutzbarkeit kritisch zu prüfen. Es gelten dieselben Kriterien wie auch in anderen Zusammenhängen bei der Suche nach wissenschaftlichen Quellen: Belegte und nachvollziehbare Ausführungen, korrektes Zitieren, vollständige Quellen- und Lizenzhinweise etc. Gleichzeitig profitieren sie von der Schwarmintelligenz und der Arbeit vieler Experten, die die Materialien erstellen und weiterentwickeln, und können selbst ebenfalls dazu beitragen.


## Lizenzen
Die Veröffentlichung von Inhalten als OER bedeutet nicht, dass das Urheberrecht nicht mehr gilt. Der Urheber entscheidet mittels der gewählten Lizenz, in welcher Weise und unter welchen Umständen seine Inhalte weiterverbreitet und bearbeitet werden dürfen. Verbreitet sind die sogenannten Creative Commons-Lizenzen. OER bedeutet nicht: „Ich kann damit machen was ich will“!

### Die „Creative Commons“ - Lizenzen
Die bekanntesten offenen Lizenzen sind die Creative Commons-Lizenzen, die mittlerweile in Version 4.0 vorliegen. Häufig abgekürzt werden sie mit CC, gefolgt von der Versionsnummer. Kennzeichnend für die CC-Lizenzen ist, dass sie gestaffelt sind, je nachdem welche Rechte den Nutzer:innen von den Autor:innen eingeräumt werden:

![Lizenzen](/images/lizenzen.jpeg)
*Abbildung: CC-Lizenzbuttons von https://www.creativecommons.org. Tabellarische Darstellung: CC BY-SA Christian Pfliegel*

Als Sonderform gibt es noch die Lizenz CC0, bei der nicht einmal eine Namensnennung des/der Urheber:in notwendig ist. In Europa ist dies eine Grauzone, da das europäische Urheberrecht nicht vorsieht, dass alle Rechte an einem Werk abgegeben werden.

Zusammenfassend ist folgendes bei den jeweiligen Lizenzen erlaubt:
Als freie Lizenzen gelten gemeinhin die Lizenzen CC0, CC BY und CC BY-SA, da nur diese wirklich frei verwendet werden dürfen und die Nutzer:innen nicht einschränken (im Gegensatz zu den NC und ND - Lizenzen).
Bei der Lizenz CC BY-NC ist zu beachten, dass mit dem non commercial nicht nur ein Verkauf des Materials gemeint ist. Diese Lizenz darf nicht in einem Umfeld verwendet werden, das im Zusammenhang mit Kommerz steht (also auch keine gewerblichen Blogs, keine freiberuflichen Vorträge, aber auch nicht im Rahmen von Gottesdiensten oder Erwachsenenbildung).
Bei CC BY-SA ist zu beachten, dass diese Lizenz „ansteckend ist“, d. h. wird Material, das unter einer CC BY-SA-Lizenz steht verwendet, muss das neu erstellte Material ebenfalls unter CC BY-SA stehen, was ggfs. eine starke Einschränkung sein kann.

![Lizenzen 2](/images/lizenzen_2.jpg)
*Abbildung: Aus „How To Attribute Creative Commons Photos“ von Foter unter CC BY-SA 3.0 auf https://foter.com/blog/how-to-attribute-creative-commons-photos/*


## Korrekte Auszeichnung von OER
Bei Nutzung von OER-Materialien ist die TULLU-Regel zu befolgen:

T = Titel
U = Urheber:in
L & L = Lizenz und Link zum Lizenztext
U = Ursprungsort

Sind Änderungen vorgenommen worden, so ist darauf hinzuweisen. Werden mehrere als OER lizenzierte Materialien in das eigene Material aufgenommen (Textbausteine, Grafiken, Fotos etc.), so ist die Lizenzierung für jedes Medium anzugeben. Idealerweise erfolgt dies direkt beim Medium, alternativ im Rahmen eines Quellennachweises.

**Wichtig:** Alle fünf Angaben sind verpflichtend. Vor allem der Link zum Lizenztext wird häufiger vergessen, was in der Vergangenheit schon zu kostenpflichtigen Abmahnungen geführt hat.

![TULLU](/images/tullu.jpg)
*Grafik von Julia Eggestein nach einem Konzept von Sonja Borski und Jöran Muuß-Merholz für OERinfo – Informationsstelle OER unter CC BY 4.0 Lizenz auf https://open-educational-resources.de/oer-tullu-regel*


## OER selbst erstellen
Soll ein eigenes Werk (egal ob originär oder unter Verwendung von OER-Materialien entstanden) als OER veröffentlich werden, ist ein CC-Lizenz zu wählen. Ein als OER veröffentlichtes Werk sollte idealerweise nur aus freien Inhalten bestehen, d.h. alle verwendeten Materialien sollten unter freier Lizenz veröffentlicht sein. Jede Abweichung (Zitate, urheberrechtlich geschützte Bilder, für die eine Nutzungsgenehmigung eingeholt wurde) muss gekennzeichnet werden, sodass ein Nutzer klar erkennen kann, welche Materialien er nicht frei weiterverwenden darf.

Es ist jedoch nicht notwendig, ein eigenes Werk als OER zu veröffentlichen, weil selbst OER-Materialien darin verwendet wurden. Es kann dann aber jeder, der Zugang zu dem Material hat, die darin verwendeten OER-Materialien seinerseits wieder laut Lizenz verwenden.

Werden eigene Werke als OER lizenziert, so richtet sich die eigene Lizenz immer nach der restriktivsten verwendeten Fremdlizenz: Steht ein Material unter der CC BY-SA-Lizenz, so wird auch das neue Werk unter CC BY-SA lizensiert. Ggf. können auch einzelne Materialien aus der Gesamtlizenz exkludiert werden, um dies zu umgehen (z. B. „Dieses Werk steht unter der Lizenz CC BY mit Ausnahme von…“ oder „Soweit nicht anders gekennzeichnet, steht das Material unter Lizenz CC BY“). Alternativ veröffentlicht man das Werk als Komposition,  wie im hier vorliegenden OER: Jedes Material wird mit seiner eigenen Lizenz versehen.

<br>

> ![Lampe](/images/light.png)

> **_Praxistipp:_** Unter diesem Link geht es zum CC-Lizenzgenerator:
https://creativecommons.org/choose/?lang=de


## Entscheidungshilfe für Autor:innen
Die richtige Lizenz für dein OER zu finden, kann ziemlich komplex sein. Die vorangegangene Grafik kann bei der Lizenzfindung eine gute Unterstützung sein.

![Entscheidungshilfe Autor:innen](/images/entscheidungshilfe.jpg)
*Infografik „Welches ist die richtige CC-Lizenz für mich?“ (Grafik: Barbara Klute und Jöran Muuß-Merholz für wb-web unter CC BY-SA 3.0 auf https://wb-web.de/material/medien/die-cc-lizenzen-im-uberblick-welche-lizenz-fur-welche-zwecke-1.html*


## Quellen für OER
Für Open Educational Resources gibt es kein zentrales Repositorium, an dem alle Materialien mit einem Klick zu finden sind, auch wenn es vereinzelt Versuche gibt, solche Plattformen aufzubauen. Allerdings sind OER in den meisten Fällen digitale Ressourcen, die im Netz auffindbar sind. Wenn sie mit entsprechenden Metadaten versehen sind, kannst du auch gezielt nach ihnen suchen.
Technisch betrachtet gibt es zwei Arten von Plattformen, auf denen Bildungsmaterialien gesucht und gefunden werden können:

Repositorien (Repositories) bezeichnen Datenbanken, auf denen Ressourcen abgelegt sind. Ein Beispiel dafür wäre ein Publikationsserver einer Bibliothek. Darüber hinaus gibt es sogenannte Referatorien (Referatories), die Metadaten und Links enthalten und somit auf die Materialien verweisen.

[Quelle: „OER in der Hochschulbildung“ von David Eckhoff, Universität Duisburg Essen, für OERinfo – Informationsstelle OER unter CC BY 4.0 auf
https://open-educational-resources.de/dossierseite/?praxis=allgemein&bereich=hochschule. Der Text wurde Julia Joachim gekürzt und von Christian Pfliegel sprachlich an das vorliegende Buch angepasst.]

### Hochschulsammlungen
Wie eingangs erwähnt, gibt es im deutschsprachigen Bereich keine zentrale Anlaufstelle, auf der man OER findet. Im internationalen Kontext lohnt sich ein Blick auf das Open Education Consortium, das die Suche nach Kursen und Kursmaterialien von hunderten Hochschulen weltweit ermöglicht.

Beispiele für Sammlungen von OER in Deutschland sind u. a. die Hamburg Open Online University (HOOU), ein Verbundprojekt aller Hochschulen im Land Hamburg, die Plattform OpenRUB von der Ruhr Universität Bochum, das zentrale OER-Repositorium des Landes Baden-Württemberg (ZOERR) und die MOOC-Plattform Mooin, die von der Firma Oncampus betrieben wird.

### Bibliotheken
Auch für Bibliotheken sind OER ein Thema, gleichzeitig können Bibliotheken mit ihrem Know-how im Bereich Metadaten einen wertvollen Beitrag zum Ausbau einer OER-Infrastruktur leisten. Ganz praktisch, und schon jetzt möglich, sind Filtermöglichkeiten nach OER-Lizenzen.

Bei der erweiterten Materialsuche der Digitalen Bibliothek Thüringen oder im Bielefeld Academic Search Engine besteht bspw. die Möglichkeit, nach Nutzungsrechten zu filtern.

### Sonstige Plattformen für OER-Bildungsmaterialien

https://www.oercommons.org: Suche nach offenen Bildungsressourcen verfeinerbar nach: Art der Lizenz, Thema, Bildungsniveau etc.; Art der Medien: Texte, Video, Audio, Simulationen, Bilder.
https://www.edutags.de: Überblick über offen zugängliche Bildungsressourcen im deutschsprachigen Raum.
https://www.bildungsserver.de/elixier/suche.html: Meta-Suchmaschine des Landesbildungsserver. Viele dieser Länderangebote bieten Verweise auf Materialien, häufig auch schon mit Auswahlmöglichkeiten für freie Lizenzen.
E-Lectures der TU Darmstadt mit CC-Lizenzen; Art der Medien: Videos, Vorlesungsfolien.
Vorlesungsaufzeichnungen der Uni Hamburg: Art der Medien: Videos.
Vorlesungsaufzeichnungen der Uni Hamburg: Art der Medien: Videos.
https://imoox.at/mooc/: viele MOOCs unter offener Lizenz.
https://www.oncampus.de/mooin: viele MOOCs unter offener Lizenz.
open.michigan: zum Finden, Benutzen, Erstellen und Teilen offener Lernressourcen; nach Fachrichtungen sortiert; Art der Medien: Texte, Vorlesungsfolien, Videos.
Yale College: Freie Lernressourcen, die nach Fachrichtungen sortiert sind.
TU Darmstadt: E-Lectures mit CC-Lizenzen. Art der Medien: Videos und Vorlesungsfolien

### Quellen für Bilder

https://www.google.de
https://ccsearch.creativecommons.org/
https://oerhoernchen.de/suche
https://beta2.oerhoernchen.de/hochschule. Betaversion für Hochschulinhalte
https://www.kurzelinks.de/oersuchen
https://www.flickr.com

> ![Lampe](/images/light.png)

> **_Praxistipp:_** Die häufig und gerne genutzten freien Bilddatenbanken Unsplash, Pixabay und diverse andere verwenden keine CC-Lizenzen, sondern eigene Lizenzvarianten. Diese Inhalte sind daher strenggenommen keine OER-Materialien.

> Darauf ist bei ihrer Verwendung Rücksicht zu nehmen! Der Grund für eine eigene Lizenz ist bei diesen Datenbanken, dass sie teilweise komplett nachgebaut wurden von Konkurrenten. Als Folge dessen ist häufig eine freie Verwendung (auch kommerziell) im Rahmen der Lizenz, der Aufbau einer eigenen, öffentlichen Datenbank aber untersagt (unter einer CC-Lizenz wäre dies erlaubt).

### Sucheinstellungen bei Google
Bei Google müssen besondere Einstellungen vorgenommen werden, um gezielt nach Materialien unter freier Lizenz zu suchen. Dabei unterscheidet sich die Suche nach Texten etwas von der Bildsuche:

**Bei Google nach Texten suchen:**
Um bei Google nach Texten zu suchen, die unter einer freien Lizenz stehen, gehe einfach so vor:

**Klicke unter der Google-Suchzeile auf „Einstellungen“, dann auf „erweiterte Suche“:**

![google1](/images/google1.jpeg)
*Screenshot und Bearbeitung: Christian Pfliegel. Alle Rechte bei Google.*


In der sich öffnenden Maske kannst du ganz unten bei „Nutzungsrechte“ wählen, nach welchen Lizenzen gesucht werden soll:

![google2](/images/google2.jpeg)
*Screenshot und Bearbeitung: Christian Pfliegel. Alle Rechte bei Google.*


**Bei Google nach Bildern suchen**
Um bei Google nach Bildern zu suchen, die unter einer freien Lizenz stehen, klicke folgendes Feld an:

![google3](/images/google3.png)
*Screenshot und Bearbeitung: Christian Pfliegel. Alle Rechte bei Google.*



## FAQ zu OER
**Muss ich ein OER zwingend öffentlich zugänglich im Internet veröffentlichen?**
Nein. Ein OER kann z.B. auch in einem geschlossenen MOOC o. ä. verwendet werden. Allerdings kann jeder, der Zugang zu dem Material erhalten hat, es entsprechend der Lizenz weiterverwenden.

**Was muss ich tun, wenn in meinem OER Inhalte stehen, die nicht frei lizenziert sind?**
Diese Inhalte müssen entsprechend gekennzeichnet sein, damit Nutzer nicht dem Eindruck unterliegen, die seien frei lizenziert. Die Nutzung der Inhalte muss urheberrechtlich abgeklärt sein, d. h. sie müssen vom Zitatrecht abgedeckt oder ihre Nutzung mit dem Urheber vereinbart sein.

**In welchem Verhältnis stehen als OER und als „Open Access“ veröffentlichte Materialien zueinander?**
Beide Konzepte sind eng miteinander verknüpft. Bei Open Access geht es vor allem darum, Inhalte lesen zu können/dürfen, also den freien (Online-)Zugang zu (wissenschaftlichen) Publikationen und Daten. Erlaubt ist dabei zunächst nur das schlichte Lesen. Wer Open Access publiziert, erklärt sich mit diesem freien Zugang einverstanden. Er kann darüber hinaus jedoch auch freie Lizenzen (beispielsweise Creative Commons) vergeben. OER laden ausdrücklich dazu ein, sie zu verändern und weiterzuverwenden. Dazu müssen sie entweder gemeinfrei sein (etwa weil der Urheberrechtsschutz abgelaufen ist) oder die Urheber haben der Allgemeinheit die Bearbeitung gestattet. In der Regel versehen sie dazu das Werk mit einer Creative-Commons-Lizenz. Von den sieben Varianten dieser Lizenz eignen sich dafür nur die CC-0 (freie Verwendung), die CC-BY (Verwendung mit Namensnennung) und die CC BY-SA (Verwendung mit Namensnennung und Weitergabe unter gleichen Bedingungen). Die übrigen Varianten schließen die kommerzielle Nutzung und/oder jegliche Veränderung aus, sodass eine kreative Auseinandersetzung mit dem Material nicht möglich ist.

**Zusammengefasst lässt sich sagen: OER = Open Access + Bearbeitungsrecht.**

**Kann ich Medien, die unter verschiedenen CC-Lizenzen stehen, zusammenbauen?**
Bedingt ja. Zunächst muss das Zusammenbauen die Inhalte wirklich so eng miteinander verbinden, dass ein durchschnittlicher Betrachter sie nicht mehr klar als getrennte Werke ansieht. Andernfalls stellt sich die Frage nicht, denn werden verschiedene Werke bloß nebeneinander präsentiert und nicht miteinander verschmolzen, liegt rechtlich gesehen meist nur eine „Werkverbindung“ vor und die Verschiedenheit der Lizenzen ist ohne Belang.

Entsteht durch das Zusammenbauen aber ein einheitlicher neuer Eindruck, hängt es von den Bedingungen der verschiedenen Lizenzen ab, ob das Ergebnis ohne weitere Erlaubnis der betroffenen Urheber bzw. Rechteinhaber genutzt werden darf.


## Vorbehalte gegen OER … und Gegenargumente
Wenn du mit OERs arbeitest oder andere von den Vorteilen überzeugen willst, wirst du sehr bald Argumente gegen offene Materialien hören, oder zumindest Nachfragen.
Um besser argumentieren zu können hier eine Zusammenstellung häufiger Vorbehalte gegen OER, sowie Argumente dagegen, die dir eventuell helfen können.
Die folgende Sammlung der häufigsten Vorbehalte kann eine ganz gute Grundlage für die Diskussion sein.

**Ich möchte meine Sachen nicht unter eine freie Lizenz stellen, da ich nicht will, dass die „falschen“ Personen mein Material einfach verwenden.**
Gegenargument: Auch bei einer restriktiveren Lizenz kannst du nicht verhindern, dass dein Material einfach verwendet wird. Selbst bei Lehrer:innen und Referent:innen ist es immer noch weit verbreitet, Materialien aus dem Internet einfach zu verwenden ohne Rücksicht auf eventuelle Copyright-Verletzungen. Die Beobachtung ist hier, dass bei offenen Lizenzen sogar häufiger auf die richtige Verwendung geachtet wird, bzw. dass das Thema Lizenz hier überhaupt thematisiert wird.

**Bei OER gibt es doch keine Qualitätssicherung?**
Stimmt. Aber auch bei anderen Quellen solltest du die Qualität prüfen. Bei OER kannst du sie sogar noch verbessern, wenn dir die Qualität nicht zusagt (solange das Material nicht unter einer ND-Lizenz steht).

**Ich habe Angst, etwas falsch zu machen bei der Lizenzierung!**
Lizenzrecht ist komplex und es lauern wirklich einige Fallstricke bei der Lizenzierung. Es ist nicht zu leugnen, dass vor einer Veröffentlichung, aber auch vor einer Nutzung, etwas Energie darauf verwendet werden muss sich mit der Thematik auseinanderzusetzen. Sehr hilfreich kann dabei die Seite https://irights.info sein, eventuell ein Kurs zu OER auf www.oncampus.de oder auch direkt die Seite www.creativecommons.org. Das Tolle an der OERs ist zudem, dass es eine lebendige und hilfsbereite Community gibt, die immer gerne Fragen beantwortet und unterstützt!

**Ich gebe doch mein geistiges Eigentum nicht einfach frei!**
Dies ist ein sehr häufiger Vorbehalt gegen die Veröffentlichung von OER. Hier ist zu sagen, dass ja jede Veröffentlichung eine Freigabe geistigen Eigentums ist, die vor allem im digitalen Raum recht einfach kopiert werden kann. Bei dieser Frage ist die Perspektive entscheidend: Es geht ja nicht darum, jedes Material, das du erstellst unter einer freien Lizenz zu veröffentlichen. Aber warum nicht einfach einzelne Teile freigeben? Das kann ja gleichzeitig eine gute Werbung für deine Arbeit und für deine Leistungen sein und beispielsweise Vernetzung mit anderen erleichtern.
In engem Zusammenhang mit diesem Vorbehalt steht auch der nächste:

**Kannibalisiere ich damit nicht mein Einkommen/meine Umsätze? Mit OER kann man doch kein Geld verdienen? Was nichts kostet, taugt auch nichts!**
Auch hier gilt: Alle Inhalte freizugeben ohne eine Anpassung des Gesamtgeschäftsplans wäre natürlich keine gute Idee. Auf der anderen Seite haben OER gutes Potential, deine Reichweite zu erhöhen und eine Vernetzung zu erleichtern, was dann zu einer Erhöhung der Umsätze führt. Häufig ist es so, dass eine reine Freigabe deiner Materialien noch lange nicht deine Kompetenz ersetzt, die ja weit über die reinen Materialien hinausgeht. Es gibt zahlreiche Beispiele für Menschen, die „Erfolg durch Teilen“ haben. Wenn OER der Grund sind, sich ausführlich mit OER und Lizenzen zu beschäftigen, hast du gleich eine Zusatzqualifikation erworben, die deine Kompetenz erhöht hat!

Auch ist zu bedenken, dass das „frei“ in „freie Materialien“ nicht von kostenlos kommt, sondern sich auf die Weiterverwendung bezieht. Solange die Materialien entsprechend lizenziert sind, spricht auch nichts gegen einen Verkauf deiner OER.
Zur Frage „Was nichts kostet, taugt auch nichts“: Deckt sich dies in Umkehrung mit deinen Erfahrungen? War es immer so, dass alles teure auch immer von bester Qualität war? Dann doch lieber kostenlos und nur vielleicht gut. Im Übrigen ist auch im Jahr 2021 das meiste im Netz immer noch frei verfübar ...

**Spart das wirklich Arbeitszeit?**
Nicht wirklich! Mittel- und langfristig kann es Zeit sparen, wenn Materialien rechtssicher und richtig lizenziert (wieder-)verwendet und angepasst werden können. Zunächst muss jedoch Zeit investiert werden, um richtig zu lizenzieren ... im Grunde genauso lange wie bei nicht-offenen Materialien. Erfahrungsgemäß wird bei diesen jedoch häufig einfach nicht auf die Lizenzen geachtet. Dies spart Zeit, sollte dann jedoch nicht als Argument gegen OER verwendet werden.


### OER - Goldstandards: Literaturtipp

![OER-Goldstandards](/images/goldstandard.png)
*Der Gold-Standard für OER, Grafik: Jula Henke, Agentur J&K – Jöran und Konsorten für OERinfo, Informationsstelle OER, CC BY 4.0.*

Vor allem für Einsteiger:innen kann die Erstellung und Veröffentlichung von OERs verwirrend sein. Je nach Art des Materials gibt es dabei unterschiedliche Fragestellungen: So besteht ein Video aus Tonspur, Video und Skript, ein Text kann in unterschiedlichen Dateiformaten veröffentlicht werden und ein kompletter Kurs hat nochmal andere Anforderungen. Zentrale Frage sind beispielsweise, wie eine Nachnutzung möglichst einfach ermöglicht wird oder wo Lizenzhinweise am besten angebracht werden.

Eine gute Hilfestellung bei solchen Fragen sind die sog. „Goldstandards für OER“, die auf OERinfo von mehreren Autor:innen erstellt wurden und die die Bereiche Texte, Arbeitsblätter, Videos, Kurse, Spiele, Webseiten und einige andere umfassen. Die Texte sind auf dieser Webseite veröffentlicht:
https://open-educational-resources.de/gold-standard-buch-artikel/
Wer die Goldstandards lieber in Buchform haben möchte kann das Buch ebenfalls auf der Webseite bestellen (Preis: knapp 8 Euro, erschienen im März 2021).
