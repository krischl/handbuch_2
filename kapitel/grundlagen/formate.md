## Formate von Bildungsangeboten
Im Bereich des digitalen Lernens können folgende Formate unterschieden werden, die jeweils unterschiedlich geplant und konzipiert werden müssen: 

#### Online-Veranstaltungen
Online-Veranstaltungen finden sehr oft synchron statt, alle Teilnehmer:innen sitzen dabei zur gleichen Zeit vor dem Rechner. Zum Einsatz kommen dabei Videokonferenztools wie Adobe Connect, Zoom, Jitsi, GoToMeeting usw. Neben dem Austausch per Video kommen oft kollaborative Möglichkeiten, wie Pinnwände, Padlets oder ähnliches zum Einsatz. 

#### Offline- oder Präsenz-Seminare: das klassische, analoge Format 
Alle Teilnehmer:innen treffen sich an einem Ort. Digital unterstützt werden kann dieses Format beispielsweise mit Feedbacktools wie Mentimeter (www.mentimeter.com) oder auch Plickers (www.plickers.com). Der Vorteil von Plickers ist, dass die Teilnehmer:innen keine Technik für die Nutzung benötigen - das Tool ist auf jeden Fall einen Blick wert! 

#### blended learning
Beim Format des blended learning handelt es sich um eine Mischform aus Online- und Präsenz: Beide Phasen finden im Wechsel statt und ergänzen sich. 

Geeignet ist diese Form z. B. für Sprachkurse: Nach einem Wochenende in Präsenz folgt eine Onlinephase, die auf die nächste Präsenzphase vorbereitet und beispielsweise mit Vokabel- oder Grammatikübungen gefüllt sein kann.

#### Hybride Formate
Im Unterschied zum blended learning finden bei hybriden Seminaren die Präsenz- und Online-Phasen nicht nacheinander statt, sondern gleichzeitig: Ein Teil der Teilnehmer:innen ist vor Ort, ein Teil ist online zugeschaltet. Dieses Format eignet sich, wenn nicht alle Teilnehmer:innen vor Ort teilnehmen können, da sie entweder zuweit entfernt sind oder weil es mehr Interessierte gibt als in den Raum dürfen. 


