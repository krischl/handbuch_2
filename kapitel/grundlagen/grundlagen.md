# Theoretische Grundlagen von Bildung in der Digitalität

Bevor in den nächsten Kapiteln praktische Tipps für die Gestaltung digitaler Bildungsformate folgen, ist es sinnvoll, einen kurzen Exkurs in die Theorie des digitalen Lernens zu unternehmen: Viele denken bei dem Begriff digitale Bildungsformate zunächst an langweilige E-Learnings, durch die man sich zwangsweise alleine, ohne persönlichen Kontakt zu anderen, gelangweilt und vielleicht nebenbei durch klickt . . . im schlimmsten Fall noch mit Technik, die nicht richtig funktioniert. Oder aber an stundenlange, langweilige Zoom-Meetings. Dieses Handbuch wird zeigen, dass digitale Bildungsformate anders sind als traditionelle, aber ebenso Spaß machen und bereichernd sein können.


