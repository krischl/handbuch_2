## ADDIE-Modell
Sehr hilfreich für die Konzeption eines digitalen Bildungsformats ist die Nutzung des sog. ADDIE-Modells. Das ADDIE-Modell ist ein im Bereich der Planung und Konzeption von Lehrumgebungen eingesetztes Instructional-System-Design-Modell, dessen Kern eine systematische Koordination der Entwicklungsphasen Analyse (=Analyze), Konzeption (=Design), Entwicklung im engeren Sinne (=Develop), Implementierung (=Implement) und Evaluation (=Evaluate) ist.

Das **ADDIE-Modell** besteht aus folgenden (Teil-)schritten: 

![ADDIE-Modell](/images/addie.jpg)
*Abbildung: Das ADDIE-Modell. CC BY-SA Christian Pfliegel*

* **Analyse:** Vor Beginn der eigentlichen Konzeption ist zu ermitteln, welchen Zweck oder Bedarf das Projekt erfüllen soll und wer die Adressat:innen sind. Didaktische Mängel in Lehrveranstaltungen sind häufig auf defizitäre Analysen der Zielgruppe zurückzuführen. 

* **Konzeption:** In dieser Phase werden die Lehrziele bereits so verbindlich formuliert, dass diese später sehr konkret überprüft werden können.

* **Entwicklung im engeren Sinne:** In diesem Schritt werden die Inhalte und Medien produziert und die benötigte Plattform wird angelegt. 

* **Implementierung:** Die entwickelten Inhalte, Materialien und Medien werden in konkreten Bildungskontexten eingesetzt, bzw. eingeführt und etabliert. Dieser Schritt ist die eigentliche Durchführung des Projekts. 

* **Evaluation:** Ein wichtiger Schritt ist die abschließende Evaluation des Projekts. Diese dient zum einen dazu, Probleme in der praktischen Umsetzung zu identifizieren und zum anderen der Messung, ob die Lehrziele, die in der Konzeptionsphase festgelegt wurden, überhaupt erreicht sind. Eine Besonderheit des ADDIE-Modells ist, dass nach jedem Entwicklungsschritt eine Evaluation erfolgt, die dann in die weitere Konzeption einfließt. 

