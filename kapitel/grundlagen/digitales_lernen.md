## Digitales Lernen
Was ist das überhaupt, dieses digitale Lernen? Unter dem Begriff digitales Lernen oder digitale Bildungsformate werden in diesem Handbuch alle Formen des elektronischen Lernens verstanden werden. Online und Offline. Der Vorteil der digitalen Formate ist eine Erweiterung der Möglichkeiten, da sie ein gewisses Maß an Orts- und Zeitunabhängigkeit mit sich bringen. Aber auch die Nachteile müssen genannt werden: 

Digitale Formate sind nicht für jeden Zweck geeignet: Für die Vermittlung von formellen Inhalten eignen sie sich in der Regel recht gut, alles Informelle, sowie der Aufbau von Netzwerken oder Beziehungen ist deutlich schwerer! 
Digitale Formate sind in der Erstellung teilweise kosten- und zeitintensiv. Vor allem bei kleineren, speziellen Zielgruppen muss der Aufwand mit dem Nutzen in Relation gesetzt werden. 
Digitale Formate sind anders, was eine Einarbeitung in vielen Fällen notwendig macht. 


## Digitales Lernen - Traditionelles Lernen
Das Lernverständnis in digitalen Formaten ist ein anderes als das in traditionellen Formaten, zusammengefasst in der Abbildung von Lisa Rosa (https://twitter.com/lisarosa):
![Traditionelles Lernverständnis vs. Digitales Lernverständnis](/images/lernen.jpeg)

*Quelle: CC-BY Lisa Rosa Link: https://shiftingschool.wordpress.com/2017/11/28/lernen-im-digitalen-zeitalter/*

Es ist wichtig, sich dieses andere Bild von Lernen vor der Konzeption einer eigenen Bildungsveranstaltung zu verinnerlichen und zu verstehen. Ein häufiger Grund für das Scheitern digitaler Formate ist die 1:1-Übertragung von traditionellen Settings auf ein digitales Setting: Ein Seminar, das online genauso umgesetzt werden soll wie ein vorher geplantes Präsenzseminar funktioniert (in aller Regel) nicht!

## Arten digitalen Lernens
Beim digitalen Lernen können synchrone (= zur gleichen Zeit am Rechner) und asynchrone (= zu beliebiger Zeit am Rechner) Formen unterschieden werden, wobei es kein besser oder schlechter, sondern nur ein passend/unpassend gibt:
Die Form muss bei der Konzeption des digitalen Bildungsformats von Anfang an mit bedacht werden und ist von verschiedenen Faktoren abhängig: 

Besteht die Möglichkeit, dass alle Teilnehmer:innen synchron, also zur gleichen Zeit am Rechner sind? Dies ist u. a. abhängig von der Stabilität der Internetverbindung des Ortes, von dem aus teilgenommen werden soll. Aber auch Zeitzonen, der Terminkalender der Teilnehmer:innen usw. haben direkten Einfluss.
Welcher Zeitrahmen ist für die Bildungsveranstaltung angesetzt? Für asynchrone Formate wird in der Regel mehr Zeit benötigt, da die Interaktionen zeitlich nicht direkt aufeinander bezogen sind. Die Teilnehmer:innen müssten ansonsten die Plattform 24 Stunden am Tag im Auge behalten.
Wie ist die technische Ausstattung der Veranstalter:innen und der Teilnehmer:innen? Synchrone Formate benötigen bessere Technik als asynchrone Formate, für die häufig ein Webbrowser reicht.
