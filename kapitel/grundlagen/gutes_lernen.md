## Was ist gutes digitales Lernen?
Was macht gutes, digitales Lernen überhaupt aus? Um diese Frage zu beantworten, bietet sich eine Hilfestellung aus dem eBildungslabor (www.ebildungslabor.de) an, die unter dem Titel „4-Felder-Analyse für gutes E-Learning“ unter einer CC BY-SA Lizenz von Nele Hirsch veröffentlicht wurde und die ich für das vorliegende Buch geremixt habe. 

Damit digitales Lernen gelingen kann sind im Vorfeld, analog zu normalen Unterricht, einige Analyseschritte notwendig:

Lehrziele: Welchen Bildungsbedarf soll das E-Learning-Projekt abdecken? Was sollen die Teilnehmer:innen am Ende der Bildungsmaßnahme wissen/können? 
Zielgruppe: Wie groß ist die Zielgruppe? Welche Motivation bringt die Zielgruppe mit? Inwieweit bestehen Vorkenntnisse zum Thema? Welche besonderen Bedingungen, z. B. Barrierefreiheit, müssen beachtet werden? Wie ist die Medienkompetenz der Zielgruppe einzuschätzen? Wie ist die Zielgruppe technisch ausgestattet? 
Bedingungen: Wie viel Zeit haben wir für die Entwicklung und Durchführung? Welche Personen können welche Kompetenzen beisteuern (technisches Wissen, Didaktik, Datenschutz)? Wie viele (vor allem zeitliche) Ressourcen haben wir für die Betreuung? 
Nachhaltigkeit: Wie viel Zeit haben wir für die Entwicklung und Durchführung? Welche Personen können welche Kompetenzen beisteuern (technisches Wissen, Didaktik, Datenschutz)? Wie viele (vor allem zeitliche) Ressourcen haben wir für die Betreuung? 

Die Schwierigkeit liegt darin, dass beim digitalen Lehren und Lernen sowie bei Online-Veranstaltungen verschiedene Ebenen wirken, die sich gegenseitig beeinflussen und die geschickt miteinander kombiniert werden müssen.

Die folgende Abbildung zeigt, dass bei der Konzeption von Online-Veranstaltungen sowohl technisches Wissen als auch pädagogisches und inhaltliches Wissen einfließen müssen.


Das vorliegende Handbuch will anhand von Beispielen aus der Praxis zeigen, wie diese Bereiche geschickt kombiniert werden können.


**Praxistipp:** 
Ein interessanter Ansatz für die Gestaltung guten Lernens sind die 4Ks oder auch die 4K-Kompetenzen. Diese umfassen: kritisches Denken, Kreativität, Kommunikation und Kollaboration. Zur Vertiefung ist dieser Blog-Eintrag von Jöran Muuss-Merholz sehr zu empfehlen: 

https://www.joeran.de/die-4k-skills-was-meint-kreativitaet-kritisches-denken-kollaboration-kommunikation/
