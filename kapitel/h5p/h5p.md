# H5P
Vorbemerkung: H5P ist ein sehr mächtiges und flexibel einsatzbares Instrument. Das vorliegende Kapitel kann deshalb auch keine komplette Einführung in H5P sein. Vielmehr soll es kurz mit Beispielen aus der Praxis vorgestellt werden um Lust auf dieses Tool zu machen. Anhand von zwei konkreten Beispielen aus der Praxis wird demonstriert, wie H5P im Rahmen von Online-Veranstaltungen genutzt werden kann.
Gute Einführungen zu H5P finden sich auf YouTube oder unter diesen beiden Links: 

H5P-Handbuch (.pdf, CC BY-SA Nele Hirsch) zur Einführung: 
https://pfliegel.net/wp-content/uploads/2021/03/H5P-Handbuch.pdf

Kurs „Einstieg in H5P“ (Onlinekurs, CC BY Nele Hirsch): 
https://www.oncampus.de/weiterbildung/moocs/einstieg-in-h5p


### Abstract
H5P ist die Abkürzung für „HTML5 Package“. Mit Hilfe von HP5 ist es relativ einfach möglich interaktive (Lern-)Inhalte ohne Programmierkenntnisse zu erstellen, weshalb es in den vergangenen Jahren immer beliebter und verbreiteter wurde. Der vorliegende Text stelle H5P vor, diskutiert die Einsatzmöglichkeiten und stellt erste Praxisbeispiele vor.

### Wie ist H5P entstanden?
*Moodle* ist ein mächtiges und flexibles Learning-Management-System (LMS). Leider ist *Moodle* „Out-of-the-box" wenig interaktiv und deshalb häufig unattraktiv. Das ursprüngliche Ziel bei der Entwicklng von H5P war es, genau diese Lücke zu schließen und Lehrkräften, bzw. Dozent:innen einen Baukasten an die Hand zu geben, mit Hilfe dessen sie sehr leicht Lerninteraktionen erstellen können.

H5P wurde mit Fördergeldern der norwegischen Regierung finanziert. Mittlerweile arbeiten weltweit Entwickler:innen daran, koordiniert wird das Projekt durch einen gemeinnützigen Verein. Die Webseite des H5P-Projekts lautet [www.h5p.org](www.h5p.org) (NICHT www.h5p.com! h5p.com ist das kommerzielle, kostenpflichtige Angebot).

### Was sind die Vorteile von H5P?
Mit H5P ist es Nicht-Programmier:innen möglich, relativ einfach (Lern-)Interaktionen zu erstellen. Vor allem im Vergleich zu anderen E-Learning-Autorentools wie *Articulate Storyline* oder *Adobe Captivate*, mit denen ebenfalls solche Interaktionen erstellt werden können, bietet H5P einige Vorteile:

- **H5P ist kostenlos:** Dadurch kann problemlos und ohne niederschwellig probiert werden, ob es überhaupt zum eigenen Workflow passt. Ein weiterer Vorteil ist, dass auch Teilnehmer:innen nach einer Einführung Inhalte erstellen können, so dass *flipped classroom*-Ansätze ohne Lizenzkosten möglich sind.
- **H5P ist Open-Source**: Da es nicht von einem kommerziellen Anbieter stammt ist sichergestellt, dass die Software auch noch in einigen Jahren laufen wird. Es kann nicht passieren, dass irgendwann eine benötigte Software oder Plattform abgeschaltet oder auf einmal teurer wird. 
- **H5P basiert auf offener Webtechnologie (html5)**: Für die Nutzung muss keine App installiert werden, alles läuft direkt im Browser auf jedem Gerät unabhängig vom Betriebssystem. Auch das Teilen und das Remixen von Inhalten durch andere wird durch diese Offenheit leichter, weshalb sich H5P wunderbar für die Erstellung von *Open Educational Resources* (OER) eignet. 

### Was sind die Nachteile von H5P?
Um späteren Enttäuschungen entgegenzuwirken ist es wichtig, sich auch der Nachteile im Vorfeld bewusst zu sein:

- **H5P ist wenig flexibel:** Die möglichen Interaktionen sind vorgeben. Wenn andere Interaktionen benötigt werden ist dies nicht möglich. Auch die Anpassung von Buttons und der Darstellung ist nicht banal (aber möglich!).
- **Eine längere Einarbeitung ist notwendig:** Die Lernkurve ist in der Regel steil, dennoch wirkt die Bedienung gerade für Anfänger:innen teils kompliziert. Dazu kommt, dass die Technik nur die eine Ebene ist - noch aufwendiger ist es die passende Interaktion für die eigenen Inhalte zu finden.

### Wie erstelle ich eigene H5P-Inhalte?
Als erste Anlaufstelle hat sich ein Besuch der Seite www.h5p.org bewährt. Dort findet sich eine Übersicht, welche Interaktionen aktuell verfügbar sind, inklusive Demos. Dies ist vor allem deshalb hilfreich, da laufend neue Interaktionen dazukommen. **Wichtig: Es muss kein Account angelegt werden**

![h5p.org.png](https://res.craft.do/user/full/a9551196-1d29-1bee-b311-360a25abd4a4/doc/4E0E7AAE-B92D-41C3-9716-C1A19798A55C/C2C90BB6-7C49-4133-BE23-3F971D77B536_2/f1m3tDOFG9OkbVUKyc9b2ycMCREzCHIGAPxBvaj55m4z/h5p.org.png)

*h5p.org. Eigener Screenshot: Christian Pfliegel*

Eigene Inhalte können auf drei Arten erstellt werden (Es ist auch problemlos möglich auf der einen Plattform anzufangen und später auf einer anderen weiterzuarbeiten. Auch die Bedienung ist immer gleich):

![h5p-editor.png](https://res.craft.do/user/full/a9551196-1d29-1bee-b311-360a25abd4a4/doc/4E0E7AAE-B92D-41C3-9716-C1A19798A55C/31429E9D-5908-4213-BCAD-AA22FA6D8C6E_2/7R8ULFfdBVajehSUsVAcIPa5HMP5MkxoIeGigmJoR0wz/h5p-editor.png)

Der H5P-Editor in *moodle* (oben) und in lumi (unten). Eigener Screenshot: Christian Pfliegel

**Lumi.education**
H5P-Inhalte lassen sich lokal und offline sehr komfortabel mit der Software lumi erstellen, die unter [lumi.education](lumi.education) kostenlos heruntergeladen werden kann.

**Moodle**
Seit Version 3.8 ist ist H5P fest in *Moodle* integriert, es muss also kein Plug-In mehr installiert werden. So können H5P-Inhalte sehr einfach direkt in *Moodle* erstellt und bereitgestellt werden. Eine interessante Möglichkeit ist, dass die Rechte in *Moodle* auch so vergeben werden, dass Teilnehmer:innen selbst H5P-Inhalte erstellen können, was didaktisch interessante Möglichkeiten eröffnet.

**Wordpress, Drupal und Co.**

Auch in Wordpress, Drupal und in vielen weiteren Content-Management-Systemen lassen sich H5P-Inhalte direkt erstellen. Hierfür die die Installation eines Plug-Ins notwenig.


## Praxisbeispiele für den Einsatz von H5P in Online-Veranstaltungen
Wie mehrfach geschrieben, gibt es gute Gründe dafür, den Teilnehmer:innen Vorträge von Referent:innen im Vorfeld zur Verfügung zu stellen, um die synchrone Onlinezeit so kurz wie möglich zu halten. 

### „Interactive Video“
Mit H5P können Aufzeichnungen, aber auch vorhandene Videoclips, sehr einfach mit Interaktionen (Texteinblendungen, Fragen zum Inhalt, Einblenden von Bildern, Links usw.) aufgewertet werden: 

![H5P: Interaktives Video](/images/h5p-interaktiv.png)

*Interaktives Video mit H5P. Eigener Screenshot: Christian Pfliegel*

Dies gilt nicht nur für eigenes Material, auch vorhandenes Videomaterial von YouTube, Vimeo und anderen Plattformen kann verwendet werden, da für eine Bearbeitung in H5P kein Download (und somit keine Copyright-Probleme) Voraussetzung ist. 

### „Memory“
Auch, oder vor allem, in Online-Formaten spielt das Thema Beziehung eine wichtige Rolle. Gleichzeitig ist es oft schwierig, dass sich die Teilnehmer:innen kennenlernen. Es hat sich gezeigt, dass die H5P-Interaktion Memory eine gute Möglichkeit für die Teilnehmer:innen ist, zumindest die Namen und die Gesichter der anderen vor einem Online-Seminar kennenzulernen: 

![H5P: Memory](/images/h5p-memory.png)

*Memory mit H5P. Eigener Screenshot: Christian Pfliegel*

**Praxistipp:**
Sobald das Memory erfolgreich gelöst wurde, wird die Zeit angezeigt. Über die Bestzeiten kann so ein kleiner Wettbewerb (vielleicht sogar mit einem kleinen Preis?) veranstaltet werden, was die Motivation und auch den Spaß der Teilnehmer:innen noch einmal steigert :-). Du kannst dir ziemlich sicher sein, dass danach die Meisten die Namen zu den Gesichtern gelernt haben!

### „Find the word“
Eine dritte Interaktion, die ich im Rahmen von Online-Veranstaltungen schon erfolgreich genutzt habe, ist „Find the word“: 

![H5P: Find the Word](/images/h5p_find.png)

*"Find the Word" mit H5P. Eigener Screenshot: Christian Pfliegel*

Bei „Find the word“ können Begriffe in einem Buchstabenfeld versteckt werden. Da auch hier die Zeit erfasst wird, die zum Lösen benötigt wird, kann auch diese Interaktion gut für einen kleinen Wettbewerb genutzt werden. In der Praxis können so z. B. Fachbegriffe versteckt werden, die im Zusammenhang mit dem Seminar stehen. Die Suche nach den Begriffen kann verbunden werden mit dem Auftrag, unbekannte Begriffe zu recherchieren. Mit dieser Methodik kann sichergestellt werden, dass die Teilnehmer:innen zu Beginn eines Online-Seminars eine vergleichbare sprachliche Basis haben. 

Im Kontext von Sprachkursen kann diese Interaktion ebenfalls gute Dienste leisten (Tipp: Für einen Sprachkurs lohnt sich auch ein Blick auf die Interaktion „Dialog Cards“!).


## Produktion von H5P-Inhalten
H5P-Inhalte laufen, wie geschrieben, direkt im Browser. Für die Produktion ist jedoch ein Programm, oder eine Plattform, unterstützend notwendig. Dabei gibt es verschiedene Wege: 

Für moodle, drupal, wordpress und mebis gibt es Plugins, in den H5P-Interaktionen direkt erstellt und auch bereitgestellt werden können. 
Auf der Seite www.einstiegh5p.org können Inhalte ohne Registrierung erstellt und auch für 6 Stunden bereitgestellt werden. Nach diesen 6 Stunden werden die Inhalte automatisch gelöscht, daher ist es wichtig, die erstellten Inhalte vorher auf deinem Rechner zu sichern. 
Auf https://apps.zum.de/apps können H5P-Inhalte nach erfolgter Registrierung erstellt und auch bereitgestellt werden. Auch Inhalte von anderen Nutzer:innen können eingesehen, verwendet und angepasst werden. 
Mit Hilfe des Programms lumi.education (https://www.lumi.education) können H5P-Inhalte nach Installation lokal erstellt, gespeichert und auch als html5 exportiert werden. Diese Option ist die derzeit komfortabelste, da sie auch offline funktioniert, keiner Registrierung notwendig ist und da die erstellten Inhalte durch den Export auch lokal bereitgestellt werden können, per Mail verschickbar sind usw.

H5P-Inhalte können, sollte dies von den Autor:innen freigegeben sein, als .h5p-Datei auf den eigenen Rechner heruntergeladen, auf der eigenen Plattform hochgeladen und bearbeitet werden (Ausnahme: Dies wurde von den Autor:innen des Inhalts unterbunden). Für einen Download einfach links unten auf „reuse“ klicken und „Speichern als h5p“ wählen: 

Wichtig: H5P-Inhalte, bzw. Dateien können nicht direkt abgespielt werden. Hierfür ist entweder ein System wie Moodle, Drupal, Wordpress oder Mebis notwendig oder ein Export in html in luma.education. 

Eine gute Übersicht, was mit welcher Plattform möglich ist, gibt dieses Schaubild (SuS = Schülerinnen und Schüler, LMS = learning managment system = moodle, mebis oder ähnlich): 
Im Folgenden soll nun Schritt für Schritt gezeigt werden, wie die drei vorgestellten Interaktionen „interactive video“, „Memory“ und „find the word“ mit luma.education produziert werden. Die ersten Schritte sind dabei unabhängig vom Inhaltstyp: 

* luma.education downloaden und installieren
* luma.education öffnen
* „create new h5p“ auswählen
* die gewünschte Interaktion auswählen: Details (bzw. Übernehmen) => Benutzen

### Produktion eines interaktiven Videos mit H5P

* Schritt 1: Titel vergeben

* Schritt 2: Video hochladen/einbetten. Hierzu einfach auf das „Pluszeichen“ klicken.

* Schritt 3: Es kann entweder ein Video hochgeladen werden oder ein Link eingebunden werden. Zum Abschluss einfach auf „Einfügen“ klicken. 

* Schritt 4: Auf „Schritt 2: Interaktionen hinzufügen“ klicken.

* Schritt 5: Mit Hilfe des Schiebereglers an die Stelle im Video springen, an der die Interaktion auftauchen soll. 

* Schritt 6: Die gewünschte Interaktion einfach in das Video ziehen und anlegen: Optional kannst du eine „Zusammenfassende Aufgabe“ anlegen und die Verhaltenseinstellungen anpassen. 

**Wichtig:**
Das Projekt immer wieder als .h5p auf dem Rechner speichern (Datei => speichern). Sobald alles fertig ist, kann das Projekt unter „Datei“ => „Export …“ als .html-Ordner gespeichert und geteilt oder veröffentlicht werden. 


**Praxistipp:**
Für alle, die bisher wenig mit E-Learning-Autor:innen-Tools gearbeitet haben, kann H5P am Anfang etwas schwierig erscheinen. Die Lernkurve ist jedoch sehr schnell und es lohnt sich. Da es ein sehr verbreitetes Tool ist, lohnt auch ein Blick in YouTube, wo es zahlreiche Videos gibt, die jede verfügbare Interaktion erklären!

### Produktion eines Memorys mit H5P

Die Produktion eines Memorys mit H5P ist ziemlich einfach: 

* Schritt 1: Titel vergeben

* Schritt 2: Bild 1 hochladen und einen Alternativtext eingeben, der beschreibt, was auf dem Bild zu sehen ist (wichtig für sehbehinderte Menschen!)

* Schritt 3: Das zugehörige Bild hochladen

* (Optionaler) Schritt 4: Einen Ton hochladen, wenn die Karte umgedreht wird (denkbar ist hier auch ein audio-visuelles Memory!)
Die Schritte 1 bis 4 so oft wiederholen, bis ein komplettes Memory erstellt ist. Tipp: Das Memory nicht zu groß machen, da dann nicht alle Karten auf eine Bildschirmseite passen, was schnell unübersichtlich wird. 

Zum Abschluss das Projekt speichern/exportieren, wie in der Interaktion vorher beschrieben.

### Produktion eines Wortsuchfelds mit H5P

Genauso schnell ist ein Wortsuchfeld („find the words“) mit H5P erstellt. Auch hierfür sind nur 3 Schritte notwendig: 

* Schritt 1: Titel eingeben

* Schritt 2: Beschreibung der Aufgabe eingeben (z. B. Finde die Wörter)

* Schritt 3: Die Begriffe eingeben, die versteckt sein sollen

Zum Abschluss nur noch speichern und exportieren, wie bei den Vorangegangenen beschrieben. 

**Praxistipp:**
Die „find the words“-Interaktion ist sehr schnell erstellt, da nur Text benötigt wird und kein Bild- oder Videomaterial. Für Anfänger:innen empfiehlt sich daher, mit dieser Interaktion zu beginnen, da hier sehr schnell ein sichtbares Erfolgserlebnis erzielt werden kann.