Handbuch: Digitale für digitale Bildungsformate
======

## Über dieses Buch

Diese Seite ist die digitale, erweiterte und aktualisierte Version meines "Handbuchs für Bildungsformate", das bei Visual Ink erschienen ist (Link zur kostenlosen Digitalversion sowie zur Kaufmöglichkeit: https://visual-books.com/handbuch-fuer-digitale-bildungsformate/).


## Nutzung des Buches

Auch diese digitale Version steht unter einer CC BY-SA-Lizenz, darf also frei geteilt, kopiert und angepasst werden, solange daraus entstehende Inhalte ebenfalls frei bleiben und solange der Autor genant wird (Mehr dazu im Abschnitt "Lizenz").
